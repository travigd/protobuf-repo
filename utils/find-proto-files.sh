#!/bin/bash
set -e

# find-proto-files.sh
# Find all of the .proto files that should be packaged for the given language.

FIND_PROTO_DIRS="$(dirname "$0")"/find-proto-dirs.sh

main() {
  dirs=$(bash "$FIND_PROTO_DIRS" "$@")

  for dir in $dirs; do
    find "$dir" -name '*.proto'
  done
}


main "$@"

