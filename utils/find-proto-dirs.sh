#!/bin/bash
set -e

# find-proto-dirs.sh
# Find all of the protobuf directories for a given language.

main() {
  root_dir="$1"
  target_lang="$2"

  if [ -z "$root_dir" ] || [ -z "$target_lang" ]; then
    echo "USAGE: $(basename "$0") <directory> <language>" 1>&2
    exit 1
  fi

  for subdir in "$root_dir"/*; do
    check_dir "$subdir" "$target_lang"
  done
}

check_dir() {
  dir="$1"
  target_lang="$2"
  # shellcheck disable=SC2013
  for lang in $(cat "$dir/.protolangs"); do
    if [ "$lang" = "$target_lang" ]; then
      echo "Found $dir for $target_lang" 1>&2
      echo "$dir"
    fi
  done
}

main "$@"

