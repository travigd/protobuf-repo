#!/usr/bin/env bash
set -e
shopt -s globstar
cd "$(dirname "$0")"

BRANCH="rust"
OUT_DIR=./build
VERSION=$(cat ../../VERSION)

# This probably doesn't work for directories with spaces in them...
rust_protobufs=$(bash ../../utils/find-proto-dirs.sh ../../proto rust)

rm -rf "$OUT_DIR"
mkdir "$OUT_DIR"

set -x

if [ -n "$CI" ]; then
  echo "Checking out branch $BRANCH..."
  upstream="git@gitlab.com:${CI_PROJECT_PATH}.git"
  cd "$OUT_DIR"
  git init
  git config user.name "Autobuilder"
  git config user.email "autobuilder@travisdeprato.me"
  git remote add upstream "$upstream"
  git fetch upstream
  git checkout -b "$BRANCH"
  git rm -rf --ignore-unmatch ./
  cd "$OLDPWD"
else
  echo "Not running in CI, skipping branch $BRANCH checkout..."
fi

sed "s/<<VERSION>>/$VERSION/g" < ./Cargo.template.toml > "$OUT_DIR"/Cargo.toml
mkdir "$OUT_DIR"/src
# shellcheck disable=SC2086
cp -r $rust_protobufs "$OUT_DIR"/src/
touch "$OUT_DIR"/src/lib.rs

if [ -n "$CI" ]; then
  echo "Pushing branch $BRANCH..."
  git_sha=$(git -C ../.. rev-parse --short HEAD)
  cd "$OUT_DIR"
  git add -A .
  git commit -m "Build based on $git_sha"
  git push -f upstream HEAD:"$BRANCH"
fi
