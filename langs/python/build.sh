#!/usr/bin/env bash
set -e
shopt -s globstar
cd "$(dirname "$0")"

OUT_DIR=./my_protobuf

# The PyPI tends to like 1.2.3+rev4 better than 1.2.3-rev4
VERSION=$(tr '-' '+' < ../../VERSION)

echo "# This file is machine generated. Edit pyproject.template.toml instead." > pyproject.toml
sed "s/<<VERSION>>/$VERSION/g" < pyproject.template.toml >> pyproject.toml

set -x
rm -rf "$OUT_DIR"
mkdir "$OUT_DIR"

if [ ! -d ".venv" ]; then
  # No root because we actually have to generate the "root" module
  poetry install --no-root
fi

proto_files=$(bash ../../utils/find-proto-files.sh ../../proto python)
# shellcheck disable=SC2086
poetry run protoc -I ../../proto --python_betterproto_out="$OUT_DIR" $proto_files
poetry build

if [ -n "$CI" ]; then
  # We could maybe(?) use Poetry to do this instead of Twine, but I don't really wanna figure it out.
  echo 'Uploading to registry...'
  export TWINE_PASSWORD=${CI_JOB_TOKEN}
  export TWINE_USERNAME=gitlab-ci-token
  poetry run python -m twine upload \
    --repository-url "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/pypi" \
    dist/*
fi
