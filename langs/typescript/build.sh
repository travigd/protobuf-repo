#!/usr/bin/env bash
set -e
shopt -s globstar
cd "$(dirname "$0")"

OUT_DIR=./lib
VERSION=$(cat ../../VERSION)

sed "s/<<VERSION>>/$VERSION/g" < package.template.json > package.json

set -x
rm -rf "$OUT_DIR"
mkdir "$OUT_DIR"

if [ ! -d "node_modules" ]; then
  # No root because we actually have to generate the "root" module
  npm ci
fi


PROTOC_GEN_TS_PATH="./node_modules/.bin/protoc-gen-ts"
PROTOC_GEN_GRPC_PATH="./node_modules/.bin/grpc_tools_node_protoc_plugin"

proto_files=$(bash ../../utils/find-proto-files.sh ../../proto typescript)
# shellcheck disable=SC2086
protoc \
  -I ../../proto \
  --plugin="protoc-gen-ts=${PROTOC_GEN_TS_PATH}" \
  --plugin=protoc-gen-grpc=${PROTOC_GEN_GRPC_PATH} \
  --js_out="import_style=commonjs,binary:${OUT_DIR}" \
  --ts_out="service=grpc-node:${OUT_DIR}" \
  --grpc_out="${OUT_DIR}" \
  $proto_files

if [ -n "$CI" ]; then
  # We could maybe(?) use Poetry to do this instead of Twine, but I don't really wanna figure it out.
  echo 'Uploading to registry...'
  echo "//gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}" > .npmrc
  echo "@${CI_PROJECT_ROOT_NAMESPACE}:registry=https://gitlab.com/api/v4/packages/npm/" >> .npmrc
  npm publish
fi
