# Protobuf Repo

Playing around with how to distribute protobufs to various languages

## Continuous integration setup

### Create deploy key
1. Create a deploy key
2. Add the public key to the repository (under _Settings > Repository_).
3. Add a CI variable of type _File_ named `DEPLOY_KEY` containing the SSH private key (should start with `-----BEGIN OPENSSH PRIVATE KEY-----`).
4. Profit.